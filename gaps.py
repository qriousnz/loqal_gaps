
from collections import defaultdict
from datetime import datetime, timedelta
import pickle
from pprint import pprint as pp
from webbrowser import open_new_tab

import folium
from matplotlib import patches as mpatches
import pylab as plt
import numpy as np

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import (area, conf, db, folium_map, utils)

## yes - could consolidate data structures better but this _is_ meant to be throw-away analytic code ;-)
GRID12_DETS_PKL = 'grid12_dets.pkl'
GRID12_NON_DRIBBLERS_PKL = 'grid12_non_dribblers.pkl'
GRID12_ACTIVE_RANGE_PKL = 'grid12_active_range.pkl'
GRID12_TRANSITIONS_PKL = 'grid12_transitions.pkl'
GRID12_COWS_PKL = 'grid12_cows.pkl'
GRID12_LOC_DETS = 'grid12_loc_dets.pkl'

PROVEN_PROB_GRID12S = {
    57278937: "Marton - 2015-06-21 barely anything",
    57250112: "Monck's Bay - 2016-06-24 thin day - missing hours?",
    57382409: ("Rotoroa Island - 2016-06-14 and 15. Should have been covered by"
        " 2015-11-05 to 2016-12-21"),
    57223216: "Westport - 2015-03-28 1 IMSI should be 7K",
    57238080: "Mayfield Cant. 2015-0620-21 should be 4-7K but 9 and 5.",
    57368650: ("Near Taupo Highway - should be 20-30K but 1,3 on 2016-08-08 and"
        " 09 (and 1232 on 07)"),
    88172508: ("Lincoln 2015-10-07 and 08 8 and 1 instead on 15-20K (and 173 on"
        " 06)"),
    57365238: ("Near Waiouru -  2017-07-15 9 instead on 15-20K (and 2932, 261 "
        "on 13 and 14)"),
    57246409: "Near SH7 Cant - 2015-06-20 3 instead of 4-6K",
    57366517: "Ohakune Mount - 2016-04-12 3 instead of 1.5 - 2.5K",
    57369899: 'Eight Mile J. 2015-10-02 1 instead of 3-5K',
    57246822: "Waiau 2016-11-14 167 then 2,2 instead of 1.2-2.5K",
    57366513: "Whakapapa Ski Field 2016-07-31 only 3 instead of 600-7K",
    57366526: "Whakapapa Knoll Ridge Cafe 2016-06-28 0 and 07-31 3 not 3-11K",
    57279929: "Pakuratahi Forks Kaitoke Regional Park 2016-07-24 67 not 4-7K",
    57191842: "Mt Egmont 2016-03-19 16 not 4-5K",
    57382346: "Kawakawa Bay 2017-01-22 83 not 2-14K",
    57166767: "Kaingaroa North 2016-01-10 9-11 had 247, 2, 614 instead of 3-5K",
    57370826: "Ruakura Livestock 2015-05-17 16-17 149,1 instead of 1300-1500",
    57268167: "Out of Nelson 2015-03-19 80 instead of 600-1200",
    57238532: "Out of Fairlie Cant 2015-06-20, 21 74,1 instead of 3-7K",
    88165009: "North of Dunedin 2015-05-26, 27 2, 0 instead of 2-4K",
    57171245: "Orongo Bay Russell 2016-01-03 to 06 61,3,2,0 instead of 6-14K",
    57172945: ("Near Waiotama i.e. nowhere 2016-09-01, 02 606,210,1,1,1129 from"
        " 08-30 to 09-03 instead of 3-4K"),
    89039866: ("SH to Te Anau 2015-11-20 6788,5,331 for 19-21 instead of "
        "1.2-1.7K"),
    57265482: ("Near Lake Grassmere i.e. waps 2016-11-15 161,3,886 14-16 "
        "instead of 1.2-6K"),
}

EXPLAINED_GRID12S = {
    57174647: ("Mangere Auckland but only 1 cell_id_rti from "
        "2015-01-01 to 2017-04-22 and then nothing"),
    57371265: ("Hampton Down race track - an early small episode (2015-01-29 to"
        " 2015-02-02) then starting for the long run later, thus the gap."),
    57370870: ("Hamilton Livestock - only 200-400ish around the period with "
        "gaps and much lower in weekends" ),
#     57368022: ("Mt Pihanga - 2015-06-25 only the transition between changing "
#         "cell_id_rti's"),
    57280141: ("Greytown (NI) Seems like actual start date was a lot later - "
        "dribbling only prior"),
#     57278495: ("Wanganui Freezing Works - 2015-09-14 only the transition "
#         "between changing cell_id_rti's"),
#     57360981: ("Tuckers Wool Scourers - 2015-10-28 only the transition "
#         "between changing cell_id_rti's"),
    57168224: "Small test run then pause till properly started?",
    57171471: "Northland Prison - only small numbers or no IMSI's allowed?",
    57242298: "Klondyke Corner DOC campsite - only a few days each Summer",
    57173774: "Ahuroa - mini-start then proper start",
    57171541: "Kauri Cliffs Lodge - not in original Remote rural Northland",
    88170196: "Macraes Mine in Otago",
    88171124: "Silver Ferns Farm - weekends lower",
    57243955: "Started slightly later than supposed to",
    57293354: "Middle of nowhere so flaky all along",
    57191703: "Fonterra Hawera plant - weekends lower, flaky",
    88166224: "North of Oamaru - flaky overall",
    57245904: ("DOC Carpark beyond Hanmer Springs 2017-05-18 to 2017-06-05 "
        "Explained - dribbling after"),
    57238754: ("Albury out of Timaru on SH 2016-05-08 to 22 (just a delayed "
        "start), 2017-03-24 to 28 2,0,1,0,0 instead of 0.6-1K"),
    57265680: "Kaikoura (Clarence) - earthquake effect",
}

def imsis_for_grid12_for_day(grid12, date_str):
    """
    For given date, get distinct IMSIs that connect to any of the cell_id_rti's
    which are associated with a grid12 on that date. Note - cell_id_rti's move
    around so grid12's don't have stable sets of cell_id_rti's that define them.
    """
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT COUNT(*) AS
    n_imsis
    FROM (
      SELECT DISTINCT IMSI_rti
      FROM {connections} AS conns
      INNER JOIN
      {cell_info} AS cell
      USING (cell_id_rti)
      WHERE conns.date = %s
      AND cell.start_date <= %s
      AND (
        (cell.end_date IS NULL)
        OR
        (cell.end_date >= %s)
      )
      AND cell.grid12 = %s
    ) AS src
    """.format(connections=conf.CONNECTION_DATA, cell_info=conf.CELL_LOCATION)
    cur_rem.execute(sql, (date_str, date_str, date_str, grid12))
    n_imsis = cur_rem.fetchone()['n_imsis']
    return n_imsis

def imsis_for_day_per_grid12(on_date_str):
    """
    Use pre-aggregated cell_summary data - link from cell_id_rti to grid12 for
    date.
    """
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT
    grid12,
      CASE
        WHEN SUM(distinct_imsi) = 0 THEN 'None'
        WHEN SUM(distinct_imsi) BETWEEN 1 AND 10 THEN 'Very few'
        WHEN SUM(distinct_imsi) BETWEEN 11 AND 100 THEN 'Few'
        WHEN SUM(distinct_imsi) > 100 THEN 'Lots'
        ELSE 'Error!'
      END AS
    rough_imsi_nums
    FROM {daily_connections} AS daily
    INNER JOIN
    {cell_info} AS loc
    USING (cell_id_rti)
    WHERE daily.date = %s
    AND loc.start_date <= %s
    AND (
      (loc.end_date IS NULL)
      OR
      (loc.end_date >= %s)
    )
    AND grid12 IS NOT NULL
    GROUP BY grid12
    """.format(daily_connections=conf.DAILY_CONNECTION_DATA,
        cell_info=conf.CELL_LOCATION)
    cur_rem.execute(sql, (on_date_str, on_date_str, on_date_str))
    data = cur_rem.fetchall()
    grid12_n_imsis = [
        {'grid12': row['grid12'], 'rough_imsi_nums': row['rough_imsi_nums']}
        for row in data]
    return grid12_n_imsis

def store_summary_derived_data(dts):
    """
    Storing rough number of imsi's per date for each grid12 so we can plot on
    heatmap.

    {58767548: {'2015-01-01': 236, '2015-01-02': 218, ...}}
    """
    grid12_dets = defaultdict(dict)
    for on_dt in dts:
        print("Looking at {}".format(on_dt.strftime('%Y-%m-%d')))
        grid12_n_imsis = imsis_for_day_per_grid12(on_dt)
        print("Just got grid12_n_imsis")
        for item_n, item in enumerate(grid12_n_imsis, 1):
            grid12 = item['grid12']
            rough_nums = item['rough_imsi_nums']
            grid12_dets[grid12][on_dt] = rough_nums
            if item_n % 100 == 0:
                print(utils.prog(item_n, len(grid12_n_imsis)))
    with open(GRID12_DETS_PKL, 'wb') as f:
        pickle.dump(grid12_dets, f)

def store_grid12_loc_dets():
    """
    For each grid12, store n_rtis (useful for crude sorting of data so we can
    tell the first grid12's are the most problematic if lacking data), and
    details useful for displaying grid12 on map (centroid coords, rect,
    cell_id_rti's).

    {58767548: {
        'n_rtis': 2,
        'rect': '... polygon details',
        'lat': -39.123,
        'lon': 174.123,
        'rti': 59012,
    }}

    Note n_rti doesn't reflect the number of cell_id_rti's at any given moment -
    just the total that a grid12 has ever had. Getting the max number on any
    given date would be a better measure but much too much work for the value
    gained (all we want is to sift the grid12's with few cell_id_rti's to the
    bottom so we know not to worry as much about the days missing any data. If
    just a few cell_id_rti's, a modest failure could take down the grid and we
    would expect that from time-to-time. But if lots of cell_id_rti's, they must
    ALL go down for the grid to go down, and that requires an explanation.
    """
    grid12_loc_dets = {}
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT DISTINCT
      cell_id_rti AS
    rti,
      tower_lat AS
    lat,
      tower_lon AS
    lon,
    grid12,
    rect
    FROM {cell_info} AS cell
    INNER JOIN
    {grid_dets} AS grid
    USING (grid12)
    ORDER BY grid12, cell_id_rti, tower_lat, tower_lon
    """.format(cell_info=conf.CELL_LOCATION, grid_dets=conf.GRID12_DETS)
    cur_rem.execute(sql)
    data = cur_rem.fetchall()
    prev_grid12 = None
    for row in data:
        grid12 = row['grid12']
        if grid12 not in grid12_loc_dets:
            grid12_loc_dets[grid12] = {
                'rect': row['rect'],
                'rtis': [{
                    'rti': row['rti'],
                    'lat': row['lat'],
                    'lon': row['lon'],
            }]}
        else:
            grid12_loc_dets[grid12]['rtis'].append({
                'rti': row['rti'],
                'lat': row['lat'],
                'lon': row['lon'],
            })
        change = (grid12 != prev_grid12 and prev_grid12 is not None)
        if change:
            n_rtis_prev = len(grid12_loc_dets[prev_grid12]['rtis'])
            grid12_loc_dets[prev_grid12]['n_rti'] = n_rtis_prev
        prev_grid12 = grid12
    ## wrap up last grid12
    n_rtis_final = len(grid12_loc_dets[grid12]['rtis'])
    grid12_loc_dets[grid12]['n_rti'] = n_rtis_final
    with open(GRID12_LOC_DETS, 'wb') as f:
        pickle.dump(grid12_loc_dets, f)

def store_non_dribblers():
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT DISTINCT grid12
    FROM {cell_info}
    WHERE cell_id_rti IN (
      SELECT cell_id_rti
      FROM {daily_tots}
      GROUP BY cell_id_rti
      HAVING SUM(distinct_imsi) > 200)
    ORDER BY grid12
    """.format(cell_info=conf.CELL_LOCATION,
        daily_tots=conf.DAILY_CONNECTION_DATA)
    cur_rem.execute(sql)
    non_dribblers = [row['grid12'] for row in cur_rem.fetchall()]
    with open(GRID12_NON_DRIBBLERS_PKL, 'wb') as f:
        pickle.dump(non_dribblers, f)

def store_active_range():
    """
    Has to be range dates for cell_id_rti's associated with grid12 _while_
    associated with that grid12. Don't care if cell_id_rti started earlier in
    another location e.g. at the other end of the country.
    """
    unused_con_rem, cur_rem = db.Pg.get_rem()
    with open(GRID12_DETS_PKL, 'rb') as f:
        grid12_dets = pickle.load(f)
    grid12s = list(grid12_dets.keys())
    active_range = {}
    sql_min_date_tpl = """\
    SELECT
        MIN(start_date) AS
      min_date,
        MAX(CASE WHEN end_date IS NULL THEN '2999-01-01' ELSE end_date END) AS
      max_date
    FROM {cell_info}
    WHERE grid12 = %s
    """.format(cell_info=conf.CELL_LOCATION)
    for grid12 in grid12s:
        cur_rem.execute(sql_min_date_tpl, (grid12, ))
        min_date, max_date = cur_rem.fetchone()
        active_range[grid12] = (min_date, max_date)
    with open(GRID12_ACTIVE_RANGE_PKL, 'wb') as f:
        pickle.dump(active_range, f)

def store_transitions():
    """
    Collect all (non-null) start and end dates.
    """
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql_tpl = """\
    SELECT DISTINCT range_date
    FROM (
      SELECT DISTINCT start_date AS range_date
      FROM {cell_info}
      WHERE grid12 = %s
        AND start_date IS NOT NULL
      UNION ALL
      SELECT DISTINCT end_date AS range_date
      FROM {cell_info}
      WHERE grid12 = %s
        AND end_date IS NOT NULL
    ) AS src
    ORDER BY range_date
    """.format(cell_info=conf.CELL_LOCATION)
    transitions = {}
    with open(GRID12_DETS_PKL, 'rb') as f:
        grid12_dets = pickle.load(f)
    grid12s = list(grid12_dets.keys())
    for grid12 in grid12s:
        cur_rem.execute(sql_tpl, (grid12, grid12))
        transitions[grid12] = [
            datetime.strptime(row['range_date'], '%Y-%m-%d').date()
            for row in cur_rem.fetchall()]
    with open(GRID12_TRANSITIONS_PKL, 'wb') as f:
        pickle.dump(transitions, f)

def store_cows():
    """
    Any of its cell_id_rti's EVER had a COW description (even when in another
    grid12).
    """
    debug = True
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT DISTINCT grid12
    FROM {cell_info}
    WHERE cell_id_rti IN (
      SELECT DISTINCT cell_id_rti
      FROM {cell_info}
      WHERE node_description LIKE '%Transportable%'
        OR node_description LIKE '%COW%'
        OR node_description LIKE '%Trentham Racecourse%'
        OR node_description LIKE '%Riccarton Racecourse%'
        OR node_description LIKE '%Field Days%'
        OR node_description LIKE '%Field days%'
        OR node_description LIKE '%Mardi Gras%'
        OR node_description LIKE '%Maadi Cup%'
        OR node_description LIKE '%Rhythm and%'
    )
    ORDER BY grid12
    """.format(cell_info=conf.CELL_LOCATION)
    cur_rem.execute(sql)
    cows = [row['grid12'] for row in cur_rem.fetchall()]
    if debug: pp(cows)
    with open(GRID12_COWS_PKL, 'wb') as f:
        pickle.dump(cows, f)

def get_dts(start_date_str='2015-01-01', rtrim_days=2, force_max=None):
    start_dt = datetime.strptime(start_date_str, '%Y-%m-%d').date()
    if force_max:
        end_dt = datetime.strptime(force_max, '%Y-%m-%d').date()
    else:
        end_dt = (datetime.today() - timedelta(days=rtrim_days)).date()
    on_dt = start_dt
    dts = []
    while on_dt <= end_dt:
        dts.append(on_dt)
        on_dt += timedelta(days=1)
    return dts

def report_gaps(dts, grid12s=None):
    use_mpl = not bool(grid12s)
    with open(GRID12_DETS_PKL, 'rb') as f:
        grid12_dets = pickle.load(f)
    with open(GRID12_ACTIVE_RANGE_PKL, 'rb') as f:
        active_range = pickle.load(f)
    with open(GRID12_TRANSITIONS_PKL, 'rb') as f:
        transitions = pickle.load(f)
    if not grid12s:
        grid12s = list(grid12_dets.keys())  ## i.e. all
        grid12s.sort()
    title = 'grid12 coverage'
    if use_mpl:
        date_strs = [dt.strftime('%Y-%m-%d') for dt in dts]
        data = []
        GREEN = 5
        rough2val = {None: 0, 'Very few': 0.5, 'Few': 2.5, 'Lots': GREEN}
        for grid12 in grid12s:
            grid12_trans_dts = transitions[grid12]
            start, end = active_range[grid12]
            earliest_dt = (
                datetime.strptime(start, '%Y-%m-%d') + timedelta(days=2)).date()
            latest_dt = (
                datetime.strptime(end, '%Y-%m-%d') - timedelta(days=2)).date()
            grid12_data = []
            for dt in dts:
                out_of_range = (dt > latest_dt or dt < earliest_dt)
                near_transition = max(
                    [trans_dt - timedelta(days=2) <=
                     dt
                     <= trans_dt + timedelta(days=2)
                     for trans_dt in grid12_trans_dts])
                if out_of_range or near_transition:  ## if near a transition best to give benefit of a doubt or too many false positives
                    cell_val = GREEN
                else:
                    rough = grid12_dets[grid12].get(dt)
                    cell_val = rough2val[rough]
                grid12_data.append(cell_val)
            data.append(grid12_data)
        plt.figure(figsize=(50, 45))
        dark_red_patch = mpatches.Patch(color='#a30a2a', label='None')
        orange_red_patch = mpatches.Patch(color='#d43429', label='Very few')
        yellow_patch = mpatches.Patch(color='#fefebb', label='Few')
        green_patch = mpatches.Patch(color='#07683a', label='Enough')
        handles = [dark_red_patch, orange_red_patch, yellow_patch, green_patch]
        Z = np.asarray(data)
        plt.matshow(Z, cmap='RdYlGn', fignum=1, aspect='auto')  ## if not fignum=1 will be in a different figure from the main one and won't follow the figsize set  -- https://stackoverflow.com/questions/43021762/matplotlib-how-to-change-figsize-for-matshow and https://stackoverflow.com/questions/10540929/figure-of-imshow-is-too-small
        plt.legend(handles=handles, title="Rough estimate of IMSI's connecting",
            loc='upper left', ncol=4, bbox_to_anchor=(0, 0, 0.25, 0.9))  ## x, y, width, height for box inside of which the legend is placed (anchored)
        plt.xlabel('Date', fontsize=2)
        ax = plt.gca()
        ax.tick_params(axis='both', which='major', length=1, width=1,
            direction='out')
        ax.set_xticks(list(range(len(date_strs))))  ## must start at 0 unless fond of off-by-one errors ;-)
        ax.set_xticklabels(date_strs, minor=False, fontsize=2, rotation=90)
        ax.set_yticks(list(range(len(grid12s))))
        ax.set_yticklabels(grid12s, minor=False, fontsize=2)
        plt.ylabel("grid12's", fontsize=2)
        plt.savefig('grid_imsi_gaps.png', bbox_inches='tight', dpi=400)
        #plt.show()
    else:
        html = [conf.HTML_START_TPL % {'title': title, 'more_styles': """
        .red {
          background-color: red;
        }
        .light-red {
          background-color: #c32f2f;
        }
        .orange {
          background-color: orange;
        }
        td, th {
          padding: 0;
          margin: 0;
          vertical-align: top;
        }
        td {
          width: 3px;
        }
        table {
          border-collapse: collapse;
          font-size: 2pt;
        }
        """},
            "<table><thead><tr><th>&nbsp;</th>", ]
        date_strs = []
        prev_month = None
        prev_year = None
        for dt in dts:
            day = dt.day
            month = dt.month
            year = dt.year
            parts = [str(day), ]
            if month != prev_month:
                parts.append(str(month))
            if year != prev_year:
                parts.append(str(year))
            date_strs.append('<br>'.join(parts))
            prev_month = month
            prev_year = year
        for date_str in date_strs:
            html.append("<th>{}</th>".format(date_str))
        html.append("</tr>\n</thead>\n<tbody>")
        rough2class = {None: 'red', 'Very few': 'light-red', 'Few': 'orange'}
        for grid12 in grid12s:
            html.append("<tr><td>{}</td>".format(grid12))
            for dt in dts:
                rough = grid12_dets[grid12].get(dt)
                cell_class = rough2class.get(rough)
                cell_class_txt = " class='{}'".format(cell_class)
                html.append("<td{}>&nbsp;</td>".format(cell_class_txt))
            html.append("</tr>")
        html.append("</table>\n</body>\n</html>")
        ## assemble
        content = "\n".join(html)
        fpath = ("/home/gps/Documents/tourism/connections/grid12_gaps.html")
        with open(fpath, 'w') as f:
            f.write(content)
        open_new_tab("file://{}".format(fpath))
    print("Finished!")

def show_grid_dets(cur_local, grid12):
    debug = False
    with open(GRID12_LOC_DETS, 'rb') as f:
        grid12_loc_dets = pickle.load(f)
    loc_dets = grid12_loc_dets[grid12]
    if debug: print(loc_dets)
    rect = loc_dets['rect']
    coord_dets = {}
    for rti_dets in loc_dets['rtis']:
        lat = rti_dets['lat']
        lon = rti_dets['lon']
        if (lat, lon) in coord_dets:
            coord_dets[(lat, lon)].append(str(rti_dets['rti']))
        else:
            coord_dets[(lat, lon)] = [str(rti_dets['rti']), ]
    map_osm = folium.Map(location=conf.NZ_CENTRE, zoom_start=7)
    folium.PolyLine(area.Area.coords_from_geom(cur_local, rect),
            color='red', weight=5, opacity=1, latlon=True, popup=None
        ).add_to(map_osm)
    for coord, rtis in coord_dets.items():
        folium_map.FoliumMap.plot_circle(map_osm, coord, radius=30,
            colour='red', fill_opacity=0.8, popup=", ".join(rtis))
    title = "grid12_{}_dets".format(grid12)
    chart_fpath = ("{}/reports/images/{}.html".format(conf.CSM_ROOT, title))
    map_url = "file://{}".format(chart_fpath)
    map_osm.save(chart_fpath)
    html = [conf.HTML_START_TPL % {
        'title': title,
        'more_styles': """\
          .mini-map {
            float: left;
          }
          .shrink {
            font-size: 4px;
          }
        """}]
    ## show cell location data
    rtis = [rti_dets['rti'] for rti_dets in loc_dets['rtis']]
    rtis_clause = db.Pg.nums2clause(rtis)
    sql = """\
    SELECT cell_id_rti, effective_start, effective_end,
      physical_address, description, tower_lat, tower_lon
    FROM {mod_p}
    WHERE cell_id_rti IN {rtis_clause}
    ORDER BY effective_start, cell_id_rti
    """.format(mod_p=conf.MOD_P_LOCATIONS, rtis_clause=rtis_clause)
    cur_local.execute(sql)
    data = cur_local.fetchall()
    html.append("<table>\n<thead>\n<tr>"
        "\n<th>cell_id_rti</th>"
        "\n<th>start</th>"
        "\n<th>end</th>"
        "\n<th>Location</th>"
        "\n<th>Description</th>"
        "\n<th>grid12</th>\n<tr></thead>\n<tbody>\n")
    rtis = set()
    for row in data:
        rti = row['cell_id_rti']
        rtis.add(rti)
        start = row['effective_start']
        end = row['effective_end']
        lat = row['tower_lat']
        lon = row['tower_lon']
        loc = row['physical_address']
        desc = row['description']
        rti_grid12 = area.Area.grid12(area.Area.s2_id(lat, lon, level=12))
        html.append("<tr>"
            "\n<td>{}</td>"
            "<td>{}</td>"
            "<td>{}</td>"
            "<td>{}</td>"
            "<td>{}</td>"
            "<td>{}</td>\n</tr>"
            .format(rti, start, end, loc, desc, rti_grid12))
    html.append("</tbody>\n</table>")
    html.append("<p>EDIT the sql below so it only includes cell_id_rti's open "
        "for the dates in question <u>in grid12 {}</u></p>".format(grid12))
    html.append("<p>SELECT date, SUM(distinct_imsi) AS crude_tot "
        "FROM lbs_agg.fact_cell_summary WHERE cell_id_rti IN ({}) GROUP BY date"
        " ORDER BY date;</p>".format(
        ",".join([str(x) for x in sorted(list(rtis))])))
    ## Add map
    html.append("""\
    <div class='mini-map'>
      <h2>{grid12}</h2>
      <iframe class='mini-map' src='{src}'
      width={chart_width}px height=1200px>
      </iframe>
    </div>
    """.format(grid12=grid12, src=map_url, chart_width=800))
    fpath = ("{}/reports/{}.html".format(conf.CSM_ROOT,
        utils.Text.filenamify(title)))
    utils.html_opened(html, fpath)

def report_exceptions(dts):
    """
    Ignore all grid12's where:
      * always green (apart from one exception - to cope with the global outage)
      * no cell_id_rti's ever have more than 200 distinct IMSI's
      * only has COW's/transportables

    Then treat days for a grid12 as green when occurring before the grid12 had
    any active cell_id_rti's at all. 

    Sort by count of distinct cell_id_rti's ever (crude but sufficient - like
    me).
    """
    debug = True
    with open(GRID12_DETS_PKL, 'rb') as f:
        grid12_dets = pickle.load(f)
    with open(GRID12_NON_DRIBBLERS_PKL, 'rb') as f:
        non_dribblers = pickle.load(f)
    with open(GRID12_ACTIVE_RANGE_PKL, 'rb') as f:
        active_range = pickle.load(f)
    with open(GRID12_TRANSITIONS_PKL, 'rb') as f:
        transitions = pickle.load(f)
    with open(GRID12_COWS_PKL, 'rb') as f:
        cows = pickle.load(f)
    with open(GRID12_LOC_DETS, 'rb') as f:
        grid12_loc_dets = pickle.load(f)
    explained_grid12s = sorted(list(EXPLAINED_GRID12S.keys()))
    gaps_make_sense = cows + explained_grid12s
    proven_probs = sorted(list(PROVEN_PROB_GRID12S.keys()))
    date_strs = [dt.strftime('%Y-%m-%d') for dt in dts]
    data = []
    GREEN = 5
    rough2val = {None: 0, 'Very few': 0.5, 'Few': 2.5, 'Lots': GREEN}
    grid12s = list(grid12_dets.keys())  ## i.e. all
    grid12s.sort(key=lambda g: grid12_loc_dets[g]['n_rti'], reverse=True)  ## will always have a value unless we haven't refreshed GRID12_LOC_DETS. It is based on ALL grid12s in cell_info so will have all that are in GRID12_DETS (which might be date filtered)
    ones_not_to_chart = gaps_make_sense + proven_probs
    candidate_grid12s = [grid12 for grid12 in grid12s
        if grid12 in non_dribblers and grid12 not in ones_not_to_chart]
    grid12s_to_use = []
    for grid12 in candidate_grid12s:
        grid12_trans_dts = transitions[grid12]
        start, end = active_range[grid12]
        earliest_dt = (
            datetime.strptime(start, '%Y-%m-%d') + timedelta(days=2)).date()
        latest_dt = (
            datetime.strptime(end, '%Y-%m-%d') - timedelta(days=2)).date()
        debug_grid12 = (grid12 == 0) if debug else None
        if debug_grid12:
            print(start, end, earliest_dt, latest_dt)
        grid12_data = []
        n_cells_in_range = 0
        for dt in dts:
            ## shrink range slightly so not getting anxious about numbers which are rising
            out_of_range = (dt > latest_dt or dt < earliest_dt)
            if debug_grid12:
                print(dt, out_of_range)
            near_transition = max(
                [trans_dt - timedelta(days=2) <=
                 dt
                 <= trans_dt + timedelta(days=2)
                 for trans_dt in grid12_trans_dts])
            if out_of_range or near_transition:  ## if near a transition best to give benefit of a doubt or too many false positives
                cell_val = GREEN 
            else:
                n_cells_in_range += 1
                rough = grid12_dets[grid12].get(dt)
                cell_val = rough2val[rough]
            grid12_data.append(cell_val)
        if not n_cells_in_range:
            continue
        ## Filter out if all green apart from the one global outage
        cell_vals = grid12_data.copy()
        try:
            cell_vals.remove(0)  ## everything has one 0
        except ValueError:
            pass
        ## a problem if occasionally flaky - not always good or commonly flaky
        n_low = len([cell for cell in cell_vals if cell < 5])
        propn_low = n_low/n_cells_in_range
        all_ok = (min(cell_vals) == 5)  ## all green apart from the global outage
        often_flaky = (propn_low > 0.3) or (n_low > 30)
        if not (all_ok or often_flaky):
            data.append(grid12_data)
            grid12s_to_use.append(grid12)
    if data:
        plt.figure(figsize=(50, 0.5))
        Z = np.asarray(data)
        plt.matshow(Z, cmap='RdYlGn', fignum=1, aspect='auto')  ## if not fignum=1 will be in a different figure from the main one and won't follow the figsize set  -- https://stackoverflow.com/questions/43021762/matplotlib-how-to-change-figsize-for-matshow and https://stackoverflow.com/questions/10540929/figure-of-imshow-is-too-small
        plt.xlabel('Date', fontsize=2)
        ax = plt.gca()
        ax.tick_params(axis='both', which='major', length=1, width=1,
            direction='out')
        ax.set_xticks(list(range(len(date_strs))))  ## must start at 0 unless fond of off-by-one errors ;-)
        ax.set_xticklabels(date_strs, minor=False, fontsize=2, rotation=90)
        ax.set_yticks(list(range(len(grid12s_to_use))))
        ax.set_yticklabels(grid12s_to_use, minor=False, fontsize=2)
        plt.ylabel("grid12's", fontsize=2)
        plt.savefig('grid_imsi_problematic_gaps.png', bbox_inches='tight',
            dpi=400)
    pp(PROVEN_PROB_GRID12S)
    #plt.show()

def main():
    refresh_summary_data_only = False
    make_fresh_src = False
    run_exception_report = False
    report_on_specific_grid12 = True

    run_total_report = False
    confirm_grid12_dt = False
    find_row = False
    if refresh_summary_data_only and make_fresh_src:
        print("Don't be a dummy - silly to have refresh_summary_data_only and "
            "make_fresh_src both set to True")
        raise Exception("Halting so run settings can be corrected")

    with open(GRID12_DETS_PKL, 'rb') as f:
        grid12_dets = pickle.load(f)

    unused_con_local, cur_local, unused_cur_local2 = db.Pg.get_local()
    dts = get_dts(force_max='2017-07-19')
    if refresh_summary_data_only:
        store_summary_derived_data(dts)  ## need to run this before check for consistency or can never proceed beyond check (will always fail) ;-)
    data_dts = set()
    for grid12 in grid12_dets.keys():
        data_dts.add(max(grid12_dets[grid12].keys()))
    #print(max(data_dts)); return
    if max(dts) != max(data_dts):
        print("Set refresh_summary_data_only to True and run again")
        raise Exception("Halting so run settings can be corrected")
    if make_fresh_src:
#         store_summary_derived_data(dts)
#         store_non_dribblers()
#         store_active_range()
        store_transitions()
#         store_cows()
#         store_grid12_loc_dets()

    if run_total_report:
        report_gaps(dts)
    
    if run_exception_report:
        report_exceptions(dts)
        
    if report_on_specific_grid12:
        show_grid_dets(cur_local, grid12=57174646)

    
    if confirm_grid12_dt:
        grid12 = 57171245
        date_str = '2015-02-05'
        n_imsis = imsis_for_grid12_for_day(grid12, date_str)
        print(n_imsis)
    if find_row:
        print(list(grid12_dets.keys())[270])
        return

if __name__ == '__main__':
    main()